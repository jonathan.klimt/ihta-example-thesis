= Background<chap:Background>

The background section explains the neccessary background for understanding your thesis. However, this does not mean that you need to replicate full lectures here, but rather extract the essential information that are related to the thesis.

== Background 1

#lorem(54)

#lorem(64)

#lorem(104)

=== Subsection 1

#lorem(154)

=== Subsection 2

#lorem(204)

== Background 2

#lorem(54)

#lorem(64)

#lorem(104)

== Background 3

#lorem(54)

#lorem(64)

#lorem(104)

== Background 4

#lorem(54)

#lorem(64)

#lorem(104)
