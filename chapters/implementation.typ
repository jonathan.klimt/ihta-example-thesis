= Implementation<chap:Implementation>

This chapter explains _How_ you have specifically implemented you approach. If you are mainly implementing a software, describe the flow and the necessary components or if you are designing hardware, a description of the hardware components and the ideas behind it is appropriate.

You do *not* need to include all your code or files you created in here. Focus on the important things and the clever ideas you had. Your source code and design files belong in the git repository anyway.

== Component 1

#lorem(308)
#lorem(147)
#lorem(90)

== Component 2

#lorem(147)
#lorem(90)
#lorem(308)

