= Evaluation<chap:Eval>

In the evaluation chapter, you need to proof that your solution does what it should do and achived the goals you set for the solution in @chap:Approach.

== Measurement setup

It is necessary to describe the setup you use for the evaluation. be it a hardware measurement setup or a software harness.
If you have some hardware setup, why not include a photo of it here.
If you use certain algorithms to process the data, describe this process here as well.

#lorem(147)
#lorem(90)

== Results<sec:results>

This is the place for the tables and graphs you might have produced.
This is not necessarily the raw data, but the relevant things that came from your data processing.

#lorem(147)
#lorem(90)
#lorem(308)

== Discussion

What are the observations that can be drawn from your measurements? What are the limitations of the results?
Are there surprising findings and if so, how can they be explained?

Some people mix this section with @sec:results, which is perfectly fine if you discuss the individual results in place. In that case, don't forget to summarize the main findings at the end.

#lorem(308)
