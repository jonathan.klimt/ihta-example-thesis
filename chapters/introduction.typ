= Introduction<chap:intro>

The Introduction should contain a motivation for the problem, a brief description of the approach and a potentially a short outlook of the results.
Often a brief overview about the structure of the whole document is also part of this section.

*Note that the headings here are just for illustration purposes. Keep the introduction short (1-2 pages) and without further substructure*

== Motivation
#lorem(54)

#lorem(64)

== Problem Statement
#lorem(110)

== Prior Work

This is not the related work section, but a brief notion on where you pick up the work that others done before helps understanding where you go with your thesis.

#lorem(100)

== Approach & Contribution

Give a brief summary on what the thesis will do. This is related to @chap:Approach, but it should be brief and on the point.
It does not hurt to explicitly mention the contributions you archived with this thesis.

#lorem(30)

== Strucuture

The remainder of the document is structured as follows: @chap:Background explains the necessary technologies and theories for understanding this work. Blah Blah

#lorem(40)
