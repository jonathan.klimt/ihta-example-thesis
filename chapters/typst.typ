#import "@preview/tablex:0.0.6": tablex, hlinex, cellx
// This chapter is just for illustration purposes. Delete it and use the included chapters for your content.
= Typst Introduction

Typst is a new markup-based typesetting system that is designed to be as powerful as LaTeX while being much easier to learn and use. Typst has:


- Built-in markup for the most common formatting tasks
- Flexible functions for everything else
- A tightly integrated scripting system
- Math typesetting, bibliography management, and more
- Fast compile times thanks to incremental compilation
- Friendly error messages in case something goes wrong

This document illustrates the basic principles, so you can write your thesis at the ACS #footnote[https://acs.eonerc.rwth-aachen.de].
You can find more information at the official documentation page: https://typst.app/docs.

It is pretty easy to format in Typst: The following text is in _italics_, *bold* text is possible as well as #underline[underlined text].
We can also #highlight[highlight] text or use in-text `monospaced text blocks`.
Take a look at the source, it is really simple.

Of course, it is also possible to add formulas in your typst documet. This is rather similar to how LaTeX is doing it:

$ Q = rho A v + C = (n(n+1)) / 2 $

If you want to go a little more elaborate, you can directly use the element:

#math.equation(
  numbering: "1.1",
  block: true,
  $ Q = rho A v + C = (n(n+1)) / 2 $
)<formula>

This allows numbering and referencing. A point we just illustrated in @formula.


== Figures and references <sec:fig_ref>

We can do all other kinds of stuff in Typst. We can add images and reference them, like @fig:tux, simple and ugly tables like @default_table.
It is in general reccomended to add `placement: top` to all figures, if the context does not require so otherwise.

Typst has support for normal tables, however, they are not that beautiful and should only be used if really needed. @default_table is an example of such a table.

#figure(
  caption: "This is a basic table. It should not be used.",
  placement: none,
  table(
    columns: (15%, 20%, 35%),
    inset: 10pt,
    align: horizon,
    [First Col], [Second Col], [Third Col],
    [1.], [Some Content], [#lorem(10)]
  )
) <default_table>

In scientific texts, one usualy does not place vertical lines, and only horizontal lines to separate sections in the table. There is a Typst package called `tablex` that provides such kind of tables. An example table is shown in @nice_table.

#figure(
  caption: "This is a table",
  kind: table,
  tablex(
    columns: (auto, auto, auto),
    align: (center, left, end),
    auto-vlines: false,
    auto-hlines: false,
    hlinex(stroke: 2pt),
    [*Nr*], [*Description*], [*Price*],
    hlinex(),
    [1], [Something], [5 €],
    [2], [Something else], [7 €],
    [3], [Completely different], [3 €],
    hlinex(),
    [], cellx(align: end, [*Sum:*]), [15 €],
    hlinex(stroke: 2pt),
  )
) <nice_table>

If we put some code block in a figure, like in @rust_hello_world, it is automatically converted to a listing. Syntax highlighting is also enabled by default.

#figure(
  caption: "A floating code snippet in a nice language.",
  placement: none,
```rust
fn main() {
    println!("Hello World!");
}
```
)<rust_hello_world>


We just add some lorem-ipsum, to enforce a line break before @fig:tux is displayed:

#text(style: "italic", lorem(100))

// This is the figure
#figure(
  image("../assets/Tux.png", width: 30%),
  placement: top,
  caption: [
    Tux, the inofficial Linux mascot.
  ],
)<fig:tux>

== Citations & References

We have already referenced figures and tables in @sec:fig_ref. A reference is created by adding the lable to the element in braces (like: `<label>`), and then using an `@` sign for the reference.

Typst also supports BibTex bibliographies. For this thesis template, you have to adapt the file `thesis.bib` in the same folder as the `thesis.typ`.

There are some example entries in that file already, so lets cite @bruegge2004object @Hoefler2010 and @Kantee14.

We can also use more advanced citations, as shown by #cite(<Hoefler2010>, supplement: "p. 42", form: "author", style: "deutsche-sprache"). However, there are still some issues regarding author citations.

Only cited entries end up in the final bibliography at the end of the document.

/////////////////////////////////////////////////////////////////////
//  End of Typst Introduction
/////////////////////////////////////////////////////////////////////
