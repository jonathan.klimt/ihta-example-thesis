= Approach<chap:Approach>

This is probably the most important  part of the document. It describes your approach and the idea behind it. It does not necessarily need to be very long (Maybe 4-5 pages for Bachelor and 5-10 pages for a master thesis, but this depends on your topic).

== Writing a Thesis in Typst

At first, it is best to give an overview of your approach. Maybe a schematic image can explain the situation best.
Present the questions you want to answer with your work and why your approach is the best to solve the problem.
Also define the requirements that such a solution needs to fulfil.

#lorem(54)

#lorem(64)

#lorem(104)

=== Component 1

Most approaches are somewhat complicated, so you can structure your text and explain parts of it separately.

#lorem(104)


=== Component 1

#lorem(207)

== Related Work

Put your work in the context of what is already out in the world.
And most important, explain what you do different than the mentioned works.

#lorem(54)

#lorem(64)

#lorem(104)
