
#set heading(numbering: none, outlined: false)
= Merkblatt für  Abschlussarbeiten <chap:merkblatt>

Die Aufgabe der Abschlussarbeit besteht darin, das gestellte Thema in der vorgesehen Zeit (i.A. 3 oder 6 Monate) zu bearbeiten und die Ergebnisse in einem Kolloquiumsvortrag und ggf. in Form eines Posters zu präsentieren.

+ *Die schriftliche Fassung* soll Zielstellung, Durchführung und Ergebnis der gestellten Aufgabe so darlegen, dass die Arbeit von einem entsprechend vorgebildeten, aber außenstehenden Leser ohne Weiteres verstanden werden kann. Beachten Sie dabei, dass Sie sich in der zurückliegenden Zeit intensiv mit der einschlägigen Materie beschäftigt haben, der Leser jedoch nicht.

+ *Die Arbeit sollte enthalten:*
  Erste Seite: Ein Titelblatt (erstellt vom Institut für Hörtechnik und Akustik)
  Zweite Seite: Ein *Themenblatt* (erstellt vom Studierenden) mit *Arbeitstitel in Deutsch und Englisch* sowie einer ca. halbseitigen *Zusammenfassung in Deutsch und Englisch* (s. Muster).
  Ein Inhaltsverzeichnis,
  eine Einleitung,
  Erläuterung der Grundlagen
  Beschreibung der Methode(n),
  (Prinzip und Aufbau der Messanordnung, Rechenverfahren, Implementierung usw.)
  Durchführung der Untersuchung,
  Darstellung der Ergebnisse,
  eine Zusammenfassung,
  ein Literaturverzeichnis

  Stand September 2022 bietet folgende Website einen zusätzlichen Überblick zur Erstellung und Gliederung wissenschaftlicher Arbeiten: https://www.phys.unsw.edu.au/~jw/thesis.html

+ *In der Einleitung soll* der Rahmen abgesteckt werden, in dem die gestellte Aufgabe zu sehen ist, außerdem soll sie über die wichtigsten der bereits vorliegenden Vorarbeiten orientieren. In diesem Teil muss die Motivation und das Ziel dargestellt werden.

+ *Wenn möglich, sollte einer einfachen Erläuterungsskizze* der Vorzug vor weitschweifigen Erklärungen gegeben werden. Auf die Ableitung von Formeln, die der Literatur entnommen sind, soll verzichtet werden. überhaupt ist jegliche Art von Weitschweifigkeit zu vermeiden. Die Seitenzahl der Arbeit wird bei der Bewertung nicht berücksichtigt! Eine knappe, auf das Wesentliche gerichtete Darstellung ist einer wortreichen, mit Wiederholungen eigener oder fremder Feststellungen versehenen Niederschrift in jedem Fall vorzuziehen.

+ *Eigene Berechnungen und Messungen* sind so zu beschreiben, dass sie von einem Fachmann nachvollzogen werden können. Die gesamte Arbeit sollte für einen Studierenden des gleichen Semesters und Studiengang zu verstehen sein.

+ *Lösungswege*, die sich im Lauf der Durchführung der Arbeit nicht als optimal oder gar als irreführend erwiesen haben und daher aufgegeben worden sind, sollten gleichwohl erwähnt werden, wenn auch vielleicht nicht in der gleichen Ausführlichkeit wie die schließlich erfolgreiche Methode oder Verfahrensweise. Auf alle Fälle muss erläutert werden, warum sie aufgegeben werden mussten.

+ *Bei der Wiedergabe von Mess- oder Rechenergebnissen* ist der graphischen Darstellung der Vorzug vor langen Zahlentabellen zu geben. Letztere sind nur dann am Platz, wenn die aufgeführten Zahlenwerte unter Umständen von Lesern der Arbeiten weiterverwendet werden könnten. Diagramme, wie auch alle anderen Abbildungen, müssen nummeriert, ausreichend beschriftet und mit einer für sich verständlichen Bildunterschrift (Legende) versehen werden, damit man sie z.B. auch - ohne im Text nachlesen zu müssen - untereinander vergleichen können. Umfangreiche Zahlentabellen, Rechenprogramme, Layouts von Platinen, Bestückungslisten, Konstruktionsunterlagen, ausführliche mathematische Ableitungen und dgl. sollen im Anhang der Arbeit wiedergegeben werden, damit beim Lesen der Arbeit der Zusammenhang deutlich erkennbar ist.

+ *Die benutzten Quellen* (Bücher, Zeitschriftenaufsätze, Normblätter, andere Arbeiten) sind an den jeweiligen Stellen (mit Seitenangabe) zu zitieren und am Schluss der Arbeit in einem Literaturverzeichnis aufzulisten; Bücher unter Angabe des Verlags und des Erscheinungsjahres.

+ *Das Ergebnis der Arbeit* (Mess- und Berechnungergebnisse, Wirkungsweise eines entwickelten Gerätes oder dgl.) soll diskutiert werden, d.h. es soll erläutert werden, welche Bedeutung das Ergebnis hat, ob es im Widerspruch zu anfangs gehegten Erwartungen oder zu den Ergebnissen anderer Autoren steht, ob man mit anderen Methoden noch bessere Resultate hätte erreichen können usw. Dabei wird man im Allgemeinen wieder auf die in der Einleitung umrissene Aufgabenstellung zurückkommen. Wenn möglich, soll der Einfluss einzelner Fehlerquellen auf das Ergebnis einer Messung (oder Berechnung) oder eines Aufbaus abgeschätzt werden, um ein Maß für die Genauigkeit und den Gültigkeitsbereich des Ergebnisses zu erhalten.

+ *Der Inhalt der Arbeit* soll natürlich "richtig" sein. Wenden Sie sich bitte an Ihren Betreuer, wenn Sie über die Richtigkeit einer Feststellung im Zweifel sind oder wenn Sie etwas nicht verstanden haben. Das soll Sie aber nicht davon abhalten, ggfs. auch Vermutungen oder Meinungen in der Arbeit zum Ausdruck zu bringen, die durch das Ergebnis nahegelegt werden, die Sie aber nicht in vollem Umfang beweisen können.

+ *Hat die Arbeit die Entwicklung einer Messeinrichtung*, eines Gerätes oder dgl. zum Gegenstand, so sind von allen gebauten Geräten Schaltpläne, eine kurze Bedienungsanleitung und andere technische Unterlagen mitzuliefern.

+ *Die Zusammenfassung* soll in knapper Form (ca. 1 Seite) noch einmal Ziel der Arbeit, Methoden u. dgl. umreißen.

+ *Bachelor- und Masterarbeiten* müssen in gebundenem Zustand abgegeben werden; bei einem Exemplar ist als 1. Blatt der vom Vorsitzenden des Prüfungsausschusses unterschriebene Themenbogen einzubinden.

+ *Zum Ende einer Abschlussarbeit* werden die Ergebnisse in einem 20- (BA) bzw. 40-minütigen (MA) Vortrag vorgestellt und diskutiert.

// Als Kurzfassung soll ein Poster erstellt werden, wo in knapper Form Aufgabenstellung, Ziel, Vorgehensweise und Ergebnis präsentiert sind.
// Nähere Angaben zu Vortrag und Poster sollen mit dem Betreuer besprochen werden.

+ *Zur Archivierung:* Alle Dateien der Abschlussarbeit müssen vor Ende (vor dem Abschlussvortrag) dem Betreuer dokumentiert übergeben werden.
