= Conclusion<chap:Conclusion>

The thesis must end with a conclusion chapter.
This chapter is typically not longer than one page and has the following structure (each about one paragraph):

+ First mention the problem you wanted to solve.
+ Second, summarize what your approach was in one, maybe two paragraphs
+ Then describe the results from the evaluation and the main findings
+ Last, give an outlook on how your results can be used from this point on and describe future work that can be don on this topic.

#lorem(90)

#lorem(80)
