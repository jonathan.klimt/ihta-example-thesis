#import "template/ihta-thesis.typ": thesis_settings


// Enter your thesis data here:
#show: thesis_settings.with(
  title: "Analysis and Design of a Thesis Document Written in the Typst Language",
  titleGerman: "Analyse und Entwurf eines in der Typst Sprache geschriebenen Thesis Dokuments",
  author: "Peter Pan",
  matrNr: "123456",
  degree: "Bachelor", // "Bachelor" or "Master"
  language: "en",     // "en" or "de"
  supervisors: ("Dr. Albert Einstein", "Dipl.Psych. Sigmund Freud (Univ. Wien)"),
  abstract_de: include("abstract_de.typ"),
  keywords_de: ("Typst", "Dokument", "LaTeX"),
  abstract_en: include("abstract_en.typ"),
  keywords_en: ("Typst", "Document", "LaTeX"),
  list_of_figures: true, // The lists here can be disabled, e.g., if you don't have tables in your thesis.
  list_of_tables: true,
  list_of_listings: true,
)


#include("chapters/merkblatt.typ")

#include("chapters/typst.typ")

#include("chapters/introduction.typ")

#include("chapters/background.typ")

#include("chapters/approach.typ")

#include("chapters/implementation.typ")

#include("chapters/evaluation.typ")

#include("chapters/conclusion.typ")
