FROM rust:slim

RUN cargo install --git https://github.com/typst/typst --tag=v0.10.0

# Ensure the Computer Modern fonts are available
RUN apt update && apt install fonts-cmu
ENV TYPST_FONT_PATHS=/usr/share/

ENTRYPOINT /bin/bash
