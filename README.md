# ~~ACS~~ IHTA Typst Thesis

Forget LaTeX! You can now write your thesis in the super-fast and convenient [typst language](https://typst.app/)!

[Download resulting pdf here](https://git-ce.rwth-aachen.de/acs/internal/templates/typst-templates/thesis-example/-/jobs/artifacts/master/raw/thesis.pdf?job=typst_build)

## Installation

1. Install typst [as described on their GitHub page](https://github.com/typst/typst#installation)
2. Install the _Computer Modern_ (CMU) fonts on your system:
  - Debian: `apt install fonts-cmu`
  - Fedora: `dnf install texlive-cm`
3. Fork this repostiory in your own namespace
4. Clone the repository to your local computer (the `--recursive` is important):
    ```sh
    cd ~/path/to/my/thesis
    git clone --recursive git@git-ce.rwth-aachen.de:/path/to/my/thesis/fork.git
    ```


## Build your document

Modify the `*.typ` files in the editor of your choice and then run

```sh
typst compile thesis.typ
# or
typst watch thesis.typ # Automatically recompiles on any file change
```

in the same folder as the [thesis.typ](./thesis.typ) file.

## Documentation & Help

- There is some example content in [thesis.typ](./thesis.typ).
- Check out the offical documentation: https://typst.app/docs
  - Especially the syntax reference: https://typst.app/docs/reference/syntax/
